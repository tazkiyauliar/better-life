package tubes.mobile.betterlife

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.FragmentTransaction
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import tubes.mobile.betterlife.fragments.*

class MainActivity : AppCompatActivity() {

//    lateinit var makananFragment: MakananFragment
//    lateinit var minumanFragment: MinumanFragment
//    lateinit var olahragaFragment: OlahragaFragment
//    lateinit var abousUsFragment: AboutUsFragment
//    lateinit var profileFragment: ProfileFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val navController = findNavController(R.id.container_fragment)

        val bottomNavigationView = findViewById<BottomNavigationView>(R.id.bottom_navigation_view)

        bottomNavigationView.setupWithNavController(navController)

//        val bottomNavigation : BottomNavigationView = findViewById(R.id.bottom_navigation_view)

//        makananFragment = MakananFragment()
//        supportFragmentManager
//            .beginTransaction()
//            .replace(R.id.container_fragment, makananFragment)

//        bottomNavigation.setOnNavigationItemReselectedListener { item ->
//            when (item.itemId){
//                R.id.makananFragment -> {
//                    makananFragment = MakananFragment()
//                    supportFragmentManager
//                        .beginTransaction()
//                        .replace(R.id.container_fragment, makananFragment)
//                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
//                        .commit()
//                }
//                R.id.minumanFragment -> {
//                    minumanFragment = MinumanFragment()
//                    supportFragmentManager
//                        .beginTransaction()
//                        .replace(R.id.container_fragment, minumanFragment)
//                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
//                        .commit()
//                }
//                R.id.olahragaFragment -> {
//                    olahragaFragment = OlahragaFragment()
//                    supportFragmentManager
//                        .beginTransaction()
//                        .replace(R.id.container_fragment, olahragaFragment)
//                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
//                        .commit()
//                }
//                R.id.aboutUsFragment -> {
//                    abousUsFragment = AboutUsFragment()
//                    supportFragmentManager
//                        .beginTransaction()
//                        .replace(R.id.container_fragment, abousUsFragment)
//                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
//                        .commit()
//                }
//                R.id.profileFragment -> {
//                    profileFragment = ProfileFragment()
//                    supportFragmentManager
//                        .beginTransaction()
//                        .replace(R.id.container_fragment, profileFragment)
//                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
//                        .commit()
//                }
//
//            }
//
//        }
    }
}