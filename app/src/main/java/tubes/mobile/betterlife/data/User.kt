package tubes.mobile.betterlife.data

data class User(
    var gambar : String? = null,
    var email : String? = null,
    var username : String? = null,
    var password : String? = null
)