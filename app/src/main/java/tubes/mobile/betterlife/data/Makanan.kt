package tubes.mobile.betterlife.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Makanan(
    var nama : String = "",
    var deskripsi : String = "",
    var gambar : String = "",
    var username : String =""
) : Parcelable