package tubes.mobile.betterlife

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import tubes.mobile.betterlife.adapter.ViewPagerAdapter
import tubes.mobile.betterlife.databinding.ActivityAddPostinganBinding
import tubes.mobile.betterlife.databinding.ActivityUnggahanBinding
import tubes.mobile.betterlife.fragments.posts.PostMakananFragment
import tubes.mobile.betterlife.fragments.posts.PostMinumanFragment
import tubes.mobile.betterlife.fragments.posts.PostOlahragaFragment

class UnggahanActivity : AppCompatActivity() {

    private lateinit var binding : ActivityUnggahanBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUnggahanBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbarUnggahan)

        binding.ivBack.setOnClickListener {
            onBackPressed()
        }

        setUpTabs()
    }

    private fun setUpTabs() {
        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(PostMakananFragment(), "Makanan")
        adapter.addFragment(PostMinumanFragment(), "Minuman")
        adapter.addFragment(PostOlahragaFragment(), "Olahraga")
        binding.viewPagerUnggahan.adapter = adapter
        binding.tabs.setupWithViewPager(binding.viewPagerUnggahan)

    }

}