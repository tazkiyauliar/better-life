package tubes.mobile.betterlife.utils

import android.content.Context

class Preference(private val context : Context) {

    val pref = context.getSharedPreferences("session", Context.MODE_PRIVATE)
    val edit = pref.edit()

    fun setValue(key : String, value : String){
        edit.putString(key, value)
        edit.commit()
    }

    fun getValues(key: String) : String? {
        return pref.getString(key, "")
    }

    fun logout(){
        edit.clear()
        edit.commit()
    }
}