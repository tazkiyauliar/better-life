package tubes.mobile.betterlife

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import tubes.mobile.betterlife.data.User
import tubes.mobile.betterlife.databinding.ActivitySignUpBinding

class SignUpActivity : AppCompatActivity() {

    private lateinit var binding : ActivitySignUpBinding
    private lateinit var database : DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySignUpBinding.inflate(layoutInflater)
        setContentView(binding.root)

        database = FirebaseDatabase.getInstance().getReference("user")

        binding.tvDaftar.setOnClickListener {
            startActivity(Intent(this, LogInActivity::class.java))
        }
        binding.btnDaftar.setOnClickListener {
            signUpAccount()
        }
    }

    private fun signUpAccount() {
        val username = binding.edUsername.text.toString()
        val email = binding.edEmail.text.toString()
        val password = binding.edPassword.text.toString()

        if (username.isEmpty()){
            binding.edUsername.error = "Field ini kosong"
        } else if(email.isEmpty()){
            binding.edEmail.error = "Field ini kosong"
        } else if(password.isEmpty()){
            binding.edPassword.error = "Field ini kosong"
        } else {
            saveDataToFirebase(username, email, password)
        }
    }

    private fun saveDataToFirebase(username: String, email: String, password: String) {
        var user = User()

        user.email = email
        user.username = username
        user.password = password

        database.child(username).setValue(user)

        val intent = Intent(this, LogInActivity::class.java)
        startActivity(intent)
    }
}