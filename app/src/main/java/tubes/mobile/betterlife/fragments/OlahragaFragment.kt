package tubes.mobile.betterlife.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.database.*
import tubes.mobile.betterlife.AddPostinganActivity
import tubes.mobile.betterlife.R
import tubes.mobile.betterlife.adapter.ListAdapter
import tubes.mobile.betterlife.data.Makanan
import tubes.mobile.betterlife.databinding.FragmentOlahragaBinding

class OlahragaFragment : Fragment() {

    private lateinit var binding: FragmentOlahragaBinding
    private lateinit var database : DatabaseReference
    private var listData = ArrayList<Makanan>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentOlahragaBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        database = FirebaseDatabase.getInstance().getReference("olahraga")
        getData()

        binding.rvOlaharaga.layoutManager = LinearLayoutManager(view.context)
        binding.rvOlaharaga.setHasFixedSize(true)

        binding.fabAdd.setOnClickListener{
            val intent = Intent(requireContext(), AddPostinganActivity::class.java)
            intent.putExtra(AddPostinganActivity.EXTRA_TYPE, "olahraga")
            view.context.startActivity(intent)
        }
    }

    private fun getData() {
        database.addValueEventListener( object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                Log.e("OlahragaFragment", error.message)
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                listData.clear()
                for (data in snapshot.children){
                    val olahraga = data.getValue(Makanan::class.java)
                    listData.add(olahraga!!)
                }

                binding.rvOlaharaga.adapter = ListAdapter(listData)

            }

        })
    }

}