package tubes.mobile.betterlife.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.database.*
import tubes.mobile.betterlife.AddPostinganActivity
import tubes.mobile.betterlife.adapter.ListAdapter
import tubes.mobile.betterlife.data.Makanan
import tubes.mobile.betterlife.databinding.FragmentMakananBinding

class MakananFragment : Fragment() {

    private lateinit var database : DatabaseReference
    private lateinit var binding : FragmentMakananBinding
    private lateinit var adapter : ListAdapter

    private lateinit var listMakanan : ArrayList<Makanan>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentMakananBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (activity != null){

            listMakanan = ArrayList()
            database = FirebaseDatabase.getInstance().getReference("makanan")
            binding.rvMakanan.layoutManager = LinearLayoutManager(view.context)
            binding.rvMakanan.setHasFixedSize(true)

            database.addValueEventListener(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    Log.e("MakananFragment", error.message)
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    listMakanan.clear()
                    for (makanan in snapshot.children) {
                        val makan = makanan.getValue(Makanan::class.java)
                        Log.d("MakananFragment", makanan.toString())
                        Log.d("MakananFragment", makan.toString())
                        listMakanan.add(makan!!)
                    }

                    binding.rvMakanan.adapter = ListAdapter(listMakanan)

                }

            })




            binding.fabAdd.setOnClickListener{
                val intent = Intent(requireContext(), AddPostinganActivity::class.java)
                intent.putExtra(AddPostinganActivity.EXTRA_TYPE, "makanan")
                view.context.startActivity(intent)
            }
        }
    }


}