package tubes.mobile.betterlife.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.database.*
import tubes.mobile.betterlife.AddPostinganActivity
import tubes.mobile.betterlife.R
import tubes.mobile.betterlife.adapter.ListAdapter
import tubes.mobile.betterlife.data.Makanan
import tubes.mobile.betterlife.databinding.FragmentMakananBinding
import tubes.mobile.betterlife.databinding.FragmentMinumanBinding

class MinumanFragment : Fragment() {

    private lateinit var binding: FragmentMinumanBinding
    private lateinit var database : DatabaseReference

    private lateinit var listMakanan : ArrayList<Makanan>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentMinumanBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        listMakanan = ArrayList()
        database = FirebaseDatabase.getInstance().getReference("minuman")
        binding.rvMinuman.layoutManager = LinearLayoutManager(view.context)
        binding.rvMinuman.setHasFixedSize(true)

        database.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                Log.e("MakananFragment", error.message)
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                listMakanan.clear()
                for (minuman in snapshot.children) {
                    val minum = minuman.getValue(Makanan::class.java)
                    Log.d("MakananFragment", minuman.toString())
                    Log.d("MakananFragment", minum.toString())

                    listMakanan.add(minum!!)

                }
                binding.rvMinuman.adapter = ListAdapter(listMakanan)
            }

        })

        binding.fabAdd.setOnClickListener {
            val intent = Intent(requireContext(), AddPostinganActivity::class.java)
                .putExtra(AddPostinganActivity.EXTRA_TYPE, "minuman")
            view.context.startActivity(intent)
        }
    }

}