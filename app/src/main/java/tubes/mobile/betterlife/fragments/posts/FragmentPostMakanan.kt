package tubes.mobile.betterlife.fragments.posts

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.database.*
import tubes.mobile.betterlife.R
import tubes.mobile.betterlife.adapter.ListAdapter
import tubes.mobile.betterlife.data.Makanan
import tubes.mobile.betterlife.databinding.FragmentSaveMakananBinding
import tubes.mobile.betterlife.utils.Preference

class FragmentPostMakanan : Fragment() {

    private lateinit var binding : FragmentSaveMakananBinding
    private lateinit var database : DatabaseReference
    private var listMakanan = ArrayList<Makanan>()
    private lateinit var preference: Preference

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentSaveMakananBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        preference = Preference(view.context)
        database = FirebaseDatabase.getInstance().getReference("makanan")

        binding.rvMakanan.layoutManager = LinearLayoutManager(view.context)

        getData()
    }

    private fun getData() {
        database.addValueEventListener(object : ValueEventListener{
            override fun onCancelled(error: DatabaseError) {
                Log.e("FragmentPostMakanan", error.message)
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                listMakanan.clear()
                for (foods in snapshot.children){
                    val food = foods.getValue(Makanan::class.java)

                    if (food?.username == preference.getValues("username")){
                        listMakanan.add(food!!)
                    }

                }

                binding.rvMakanan.adapter = ListAdapter(listMakanan)
            }

        })
    }

}