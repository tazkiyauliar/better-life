package tubes.mobile.betterlife.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView

import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
//import kotlinx.android.synthetic.fragment_profile.*
import com.google.android.material.tabs.TabLayout
import com.google.firebase.database.*

import tubes.mobile.betterlife.EditProfileActivity
import tubes.mobile.betterlife.LogInActivity
import tubes.mobile.betterlife.R

import tubes.mobile.betterlife.*
import tubes.mobile.betterlife.adapter.ViewPagerAdapter
import tubes.mobile.betterlife.data.User
import tubes.mobile.betterlife.databinding.ActivityAddPostinganBinding
import tubes.mobile.betterlife.databinding.FragmentMakananBinding
import tubes.mobile.betterlife.databinding.FragmentProfileBinding
import tubes.mobile.betterlife.utils.Preference

class ProfileFragment : Fragment() {

    private lateinit var binding : FragmentProfileBinding
    private lateinit var database : DatabaseReference
    private lateinit var preference: Preference



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentProfileBinding.inflate(inflater, container, false)
        return binding.root

//        val v =  inflater.inflate(R.layout.fragment_profile, container, false)
//        val posts = v.findViewById<LinearLayout>(R.id.linlayPosts)
//        val saves = v.findViewById<LinearLayout>(R.id.linlaySaves)
//
//        posts.setOnClickListener {
//            val postsFragment = FragmentUnggahan()
//            val transaction: FragmentTransaction = requireFragmentManager().beginTransaction()
//            transaction.replace(R.id.profileFragment,postsFragment)
//            transaction.commit()
//        }
//
//        saves.setOnClickListener {
//            val savesFragment = FragmentTersimpan()
//            val transaction: FragmentTransaction = requireFragmentManager().beginTransaction()
//            transaction.replace(R.id.profileFragment,savesFragment)
//            transaction.commit()
//        }


//        return v

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        preference = Preference(view?.context!!)
        database = FirebaseDatabase.getInstance().getReference("user")
        val posts = view.findViewById<LinearLayout>(R.id.linlayPosts)
        var namaLengkap = view.findViewById<TextView>(R.id.namaLengkap)
        val editProfile = view.findViewById<Button>(R.id.btn_editProfil)
        val btnLogout = view.findViewById<ImageView>(R.id.logout)

        namaLengkap.text = preference.getValues("username")

        btnLogout.setOnClickListener {
            preference.logout()
            startActivity(Intent(view.context, LogInActivity::class.java))
        }

//        posts.setOnClickListener {
//            val postsFragment = FragmentUnggahan()
//            val transaction: FragmentTransaction = requireFragmentManager().beginTransaction()
//            transaction.replace(R.id.profileFragment, postsFragment)
//            transaction.commit()
//        }

        editProfile.setOnClickListener {
            val intent = Intent(requireContext(), EditProfileActivity::class.java)
            startActivity(intent)


        }

        binding.btnLihatUnggahan.setOnClickListener{
            val intent = Intent(requireContext(), UnggahanActivity::class.java)
            startActivity(intent)
        }



        binding.btnEditProfil.setOnClickListener {
            val intent = Intent(requireContext(), EditProfileActivity::class.java)
            startActivity(intent)
        }



        readData()
    }

    private fun readData() {
        database.child(preference.getValues("username").toString()).addValueEventListener(
            object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    Log.e("ProfileFragment", error.message)
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    val user = snapshot.getValue(User::class.java)
                    binding.namaLengkap.text = user?.username
                    Glide.with(this@ProfileFragment)
                        .load(user?.gambar)
                        .circleCrop()
                        .into(binding.imgProfile)
                }

            }
        )
    }
}