package tubes.mobile.betterlife

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import tubes.mobile.betterlife.databinding.ActivityEditProfilBinding
import tubes.mobile.betterlife.databinding.ActivityUnggahanBinding

class EditProfilActivity : AppCompatActivity() {

    private lateinit var binding : ActivityEditProfilBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profil)

        binding = ActivityEditProfilBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbarEditProfil)

        binding.ivBack.setOnClickListener {
            onBackPressed()
        }
    }
}