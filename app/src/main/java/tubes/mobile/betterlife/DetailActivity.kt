package tubes.mobile.betterlife

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import tubes.mobile.betterlife.data.Makanan
import tubes.mobile.betterlife.databinding.ActivityDetailBinding

class DetailActivity : AppCompatActivity() {

    private lateinit var binding : ActivityDetailBinding
    private lateinit var data : Makanan

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        data = intent.getParcelableExtra<Makanan>("EXTRA_DATA") as Makanan

        with(binding){
            tvTitle.text = data.nama
            tvDeskripsi.text = data.deskripsi
            Glide.with(this@DetailActivity)
                .load(data.gambar)
                .into(ivPhoto)
        }

        binding.ivBack.setOnClickListener {
            onBackPressed()
        }

    }
}