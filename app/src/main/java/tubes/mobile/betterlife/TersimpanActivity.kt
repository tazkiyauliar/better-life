package tubes.mobile.betterlife

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import tubes.mobile.betterlife.adapter.ViewPagerAdapter
import tubes.mobile.betterlife.databinding.ActivityTersimpanBinding
import tubes.mobile.betterlife.databinding.ActivityUnggahanBinding
import tubes.mobile.betterlife.fragments.posts.PostMakananFragment
import tubes.mobile.betterlife.fragments.posts.PostMinumanFragment
import tubes.mobile.betterlife.fragments.posts.PostOlahragaFragment

class TersimpanActivity : AppCompatActivity() {

    private lateinit var binding : ActivityTersimpanBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tersimpan)

        binding = ActivityTersimpanBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbarTersimpan)

        binding.ivBack.setOnClickListener {
            onBackPressed()
        }

        setUpTabs()


    }
    private fun setUpTabs() {
        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(PostMakananFragment(), "Makanan")
        adapter.addFragment(PostMinumanFragment(), "Minuman")
        adapter.addFragment(PostOlahragaFragment(), "Olahraga")
        binding.viewPagerTersimpan.adapter = adapter
        binding.tabs.setupWithViewPager(binding.viewPagerTersimpan)


    }
}