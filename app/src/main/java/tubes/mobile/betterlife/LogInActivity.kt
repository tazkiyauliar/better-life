package tubes.mobile.betterlife

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.database.*
import tubes.mobile.betterlife.data.User
import tubes.mobile.betterlife.databinding.ActivityLogInBinding
import tubes.mobile.betterlife.utils.Preference

class LogInActivity : AppCompatActivity() {

    private lateinit var binding : ActivityLogInBinding
    private lateinit var database : DatabaseReference
    private lateinit var preferences : Preference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLogInBinding.inflate(layoutInflater)
        setContentView(binding.root)

        preferences = Preference(this)
        database = FirebaseDatabase.getInstance().getReference("user")

        if (preferences.getValues("login").equals("1")){
            finishAffinity()
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        binding.tvDaftar.setOnClickListener {
            startActivity(Intent(this, SignUpActivity::class.java))
        }

        binding.btnMasuk.setOnClickListener {
            login()
        }

    }

    private fun login() {
        var email = binding.edEmail.text.toString()
        var password = binding.edPassword.text.toString()

        if (email.isEmpty()){
            binding.edEmail.error = "Field ini kosong"
        } else if (password.isEmpty()){
            binding.edPassword.error = "Field ini kosong"
        } else {
            saveToFirebase(email, password)
        }
    }

    private fun saveToFirebase(email: String, password: String) {
        database.addValueEventListener( object : ValueEventListener{
            override fun onCancelled(error: DatabaseError) {
                Log.e("LoginActivity", error.message)
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                for (users in snapshot.children){
                    var user = users.getValue(User::class.java)

                    if (email.equals(user?.email) && password.equals(user?.password)) {
                        Toast.makeText(this@LogInActivity, "Login Success", Toast.LENGTH_SHORT).show()

                        preferences.setValue("username", user?.username.toString())
                        preferences.setValue("login", "1")

                        finishAffinity()
                        val intent = Intent(this@LogInActivity, MainActivity::class.java)
                        startActivity(intent)
                    }
                }
            }

        })
    }
}