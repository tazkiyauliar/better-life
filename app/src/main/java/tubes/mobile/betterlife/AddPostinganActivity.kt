package tubes.mobile.betterlife

import android.R.attr
import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import tubes.mobile.betterlife.data.Makanan
import tubes.mobile.betterlife.databinding.ActivityAddPostinganBinding
import tubes.mobile.betterlife.utils.Preference
import java.io.IOException
import java.util.*


class AddPostinganActivity : AppCompatActivity() {

    companion object{
        const val EXTRA_TYPE = "type"
        private val PICK_IMAGE_REQUEST = 22
    }

    private lateinit var binding : ActivityAddPostinganBinding
    private lateinit var databaseRef : DatabaseReference
    private lateinit var storage : StorageReference
    private lateinit var preference: Preference

    private lateinit var reference : String
    private lateinit var fileUri : Uri

    private var statusAdd = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddPostinganBinding.inflate(layoutInflater)
        setContentView(binding.root)

        preference = Preference(this)

        reference = intent.getStringExtra(EXTRA_TYPE) as String

        databaseRef = FirebaseDatabase.getInstance().getReference(reference)
        storage = FirebaseStorage.getInstance().reference

        binding.ivPhoto.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(
                    Intent.createChooser(
                            intent,
                            "Select Image from here..."),
                    PICK_IMAGE_REQUEST)
        }

        binding.ivBack.setOnClickListener { onBackPressed() }

        binding.button.setOnClickListener {
            val judul = binding.edtJudul.text.trim().toString()
            val deskripsi = binding.edtPostingan.text.trim().toString()

            if (judul.isEmpty()){
                binding.edtJudul.error = "Field ini kosong"
            } else if(deskripsi.isEmpty()){
                binding.edtPostingan.error = "Field ini kosong"
            } else if(fileUri != null){
                var progressDialog = ProgressDialog(this)
                progressDialog.setTitle("Uploading..")
                progressDialog.show()

                val ref = storage.child("postingan/"+UUID.randomUUID().toString())
                ref.putFile(fileUri)
                        .addOnSuccessListener {
                            progressDialog.dismiss()
                            Toast.makeText(this, "Uploaded", Toast.LENGTH_SHORT).show()

                            ref.downloadUrl.addOnSuccessListener {
                                saveToFirebase(it.toString(), judul, deskripsi)
                            }
                        }
                        .addOnFailureListener{
                            progressDialog.dismiss()
                            Toast.makeText(this, "Failed Upload image", Toast.LENGTH_SHORT).show()
                        }
                        .addOnProgressListener{
                            var progress = 100.0 * it.bytesTransferred / it.totalByteCount
                            progressDialog.setMessage("Upload ${progress.toInt()}%")
                        }
            }
        }
    }

    private fun saveToFirebase(uri: String, judul : String, deskripsi : String) {
        var makanan = Makanan()
        makanan.nama = judul
        makanan.deskripsi = deskripsi
        makanan.gambar = uri
        makanan.username = preference.getValues("username").toString()

        databaseRef.child(judul).setValue(makanan).addOnSuccessListener {
            Toast.makeText(this, "Success upload data", Toast.LENGTH_SHORT).show()
        }
        FirebaseDatabase.getInstance().getReference("user")
            .child(preference.getValues("username").toString())
            .child(reference)
            .child(judul).setValue(makanan).addOnSuccessListener {
                onBackPressed()
            }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode === PICK_IMAGE_REQUEST && resultCode === Activity.RESULT_OK
                && attr.data != null && data?.data!! != null) {

            // Get the Uri of data
            fileUri = data?.data!!
            try {

                // Setting image on image view using Bitmap
                val bitmap = MediaStore.Images.Media
                        .getBitmap(
                                contentResolver,
                                fileUri)
//                binding.ivPhoto.setImageBitmap(bitmap)
                Glide.with(this)
                        .load(bitmap)
                        .circleCrop()
                        .into(binding.ivPhoto)
            } catch (e: IOException) {
                // Log the exception
                e.printStackTrace()
            }
        }
    }

}