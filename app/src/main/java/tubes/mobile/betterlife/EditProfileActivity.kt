package tubes.mobile.betterlife

import android.R
import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import tubes.mobile.betterlife.data.User
import tubes.mobile.betterlife.databinding.ActivityEditProfileBinding
import tubes.mobile.betterlife.utils.Preference
import java.io.IOException
import java.util.*

class EditProfileActivity : AppCompatActivity() {

    companion object{
        private val PICK_IMAGE_REQUEST = 22
    }

    private lateinit var binding : ActivityEditProfileBinding
    private lateinit var database : DatabaseReference
    private lateinit var storage : StorageReference
    private lateinit var preference : Preference

    //atribut
    private lateinit var fileUri : Uri

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEditProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbarEditProfil)

        preference = Preference(this)
        database = FirebaseDatabase.getInstance().getReference("user")
        storage = FirebaseStorage.getInstance().reference

        binding.ivAddPhoto.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(
                Intent.createChooser(
                    intent,
                    "Select Image from here..."),
                PICK_IMAGE_REQUEST
            )
        }

        binding.ivBack.setOnClickListener {
            onBackPressed()
        }

        binding.btnSaveEdit.setOnClickListener {
            updateDataUser()
        }

        readData()
    }

    private fun readData(){
        database.child(preference.getValues("username").toString()).addValueEventListener(
            object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    Log.e("EditProfileActivity", error.message)
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    val user = snapshot.getValue(User::class.java)

                    with(binding){
                        edtNama.setText(user?.username)
                        edtEmail.setText(user?.email)
                        edtPwLama.setText(user?.password)


                            Glide.with(this@EditProfileActivity)
                                .load(user?.gambar)
                                .circleCrop()
                                .into(ivPhoto)
                    }

                }

            }
        )
    }

    private fun updateDataUser() {
        val nama = binding.edtNama.text.toString().trim()
        val email = binding.edtEmail.text.toString().trim()
        val password = binding.edtPwLama.text.toString().trim()

        if (nama.isEmpty()){
            binding.edtNama.error = "Field ini kosong"
        } else if (email.isEmpty()){
            binding.edtEmail.error = "Field ini kosong"
        } else if (password.isEmpty()){
            binding.edtPwLama.error = "Field ini kosong"
        } else if  (fileUri != null){
            var progressDialog = ProgressDialog(this)
            progressDialog.setTitle("Uploading..")
            progressDialog.show()

            val ref = storage.child("profile/"+ UUID.randomUUID().toString())
            ref.putFile(fileUri)
                .addOnSuccessListener {
                    progressDialog.dismiss()
                    Toast.makeText(this, "Uploaded", Toast.LENGTH_SHORT).show()

                    ref.downloadUrl.addOnSuccessListener {
                        saveToFirebase(it.toString(), nama, email, password)
                    }
                }
                .addOnFailureListener{
                    progressDialog.dismiss()
                    Toast.makeText(this, "Failed Upload image", Toast.LENGTH_SHORT).show()
                }
                .addOnProgressListener{
                    var progress = 100.0 * it.bytesTransferred / it.totalByteCount
                    progressDialog.setMessage("Upload ${progress.toInt()}%")
                }
        }
    }

    private fun saveToFirebase(
        uri: String,
        nama: String,
        email: String,
        password: String
    ) {
        val user = hashMapOf<String, Any>(
            "email" to email,
            "username" to nama,
            "password" to password,
            "gambar" to uri
        )

        database.child(preference.getValues("username").toString()).updateChildren(user)
            .addOnSuccessListener {
                Toast.makeText(this, "Update Profile success", Toast.LENGTH_SHORT).show()

                onBackPressed()
            }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode === PICK_IMAGE_REQUEST && resultCode === Activity.RESULT_OK
            && R.attr.data != null && data?.data!! != null) {

            // Get the Uri of data
            fileUri = data?.data!!
            try {

                // Setting image on image view using Bitmap
                val bitmap = MediaStore.Images.Media
                    .getBitmap(
                        contentResolver,
                        fileUri)
//                binding.ivPhoto.setImageBitmap(bitmap)
                Glide.with(this)
                    .load(bitmap)
                    .circleCrop()
                    .into(binding.ivPhoto)
            } catch (e: IOException) {
                // Log the exception
                e.printStackTrace()
            }
        }
    }
}
