package tubes.mobile.betterlife.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
//import tubes.mobile.betterlife.fragments.FragmentSaveMakanan
//import tubes.mobile.betterlife.fragments.FragmentSaveMinuman
//import tubes.mobile.betterlife.fragments.FragmentSaveOlahraga
import java.util.ArrayList

class ViewPagerAdapter(supportFragmentManager: FragmentManager):
    FragmentPagerAdapter(supportFragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val mFragmentList = ArrayList<Fragment>()
    private val mFragmentTitleList = ArrayList<String>()

    override fun getCount(): Int {
        return mFragmentList.size
    }

    override fun getItem(position: Int): Fragment {
        return mFragmentList[position]
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return mFragmentTitleList[position]
    }

    fun addFragment(fragment: Fragment, title: String){
        mFragmentList.add(fragment)
        mFragmentTitleList.add(title)
    }


//    override fun getItemCount(): Int {
//        return 3
//    }
//
//    override fun createFragment(position: Int): Fragment {
//    }

//    override fun createFragment(position: Int): Fragment {
//        return when(position){
//            0->{
//                FragmentSaveMakanan()
//            }
//            1->{
//                FragmentSaveMinuman()
//            }
//            2->{
//                FragmentSaveOlahraga()
//            }
//            else->{
//                Fragment()
//            }
//        }
//    }

}