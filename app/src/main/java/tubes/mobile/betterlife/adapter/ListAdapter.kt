package tubes.mobile.betterlife.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import tubes.mobile.betterlife.DetailActivity
import tubes.mobile.betterlife.R
import tubes.mobile.betterlife.data.Makanan
import tubes.mobile.betterlife.databinding.ListItemBinding

class ListAdapter (private val listData : ArrayList<Makanan>) : RecyclerView.Adapter<ListAdapter.ListViewHolder>() {

    inner class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        private val binding = ListItemBinding.bind(itemView)
        fun bind(makanan: Makanan){
            with(binding){
                tvTitle.text = makanan.nama
                tvDeskrpsi.text  = makanan.deskripsi
                Glide.with(itemView.context)
                    .load(makanan.gambar)
                    .into(ivImage)

                itemView.setOnClickListener {
                    val intent = Intent(itemView.context, DetailActivity::class.java)
                    intent.putExtra("EXTRA_DATA", makanan)
                    itemView.context.startActivity(intent)

                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListAdapter.ListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
        return ListViewHolder(view)
    }

    override fun getItemCount(): Int = listData.size

    override fun onBindViewHolder(holder: ListAdapter.ListViewHolder, position: Int) {
        holder.bind(listData[position])
    }

}